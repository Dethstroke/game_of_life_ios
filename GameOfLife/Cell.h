//
//  Cell.h
//  GameOfLife
//
//  Created by Hoang and Hugo on 5/11/17.
//  Copyright (c) 2017 ___SJSULAB___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cell : NSObject
@property int x;
@property int y;
@property (nonatomic, readwrite) BOOL life_status;
@property (nonatomic, readwrite)BOOL next_status;

@end
