//
//  DrawView.m
//  GameOfLife
//
//  Created by Hoang on 5/5/17.
//  Copyright (c) 2017 ___SJSULAB___. All rights reserved.
//

#import "DrawView.h"
#import "Cell.h"

@implementation DrawView
@synthesize cellTouch;
int num = 0;

Cell *grid[14][14];


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        // Set all cell are die
        for(int i=0; i<14;i++){
            
            for(int j=0; j<14; j++)
            {
                grid[i][j].life_status = false;
            }
        }
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    // Draw grid horizontal
    for(int i=0; i<15;i++){
        CGContextMoveToPoint(context,    15, 35+i*20 );
        CGContextAddLineToPoint(context, 295, 35+i*20 );
    }
    // Draw grid vertical
    for(int i=0; i<15;i++){
        CGContextMoveToPoint(context,    15+i*20, 35 );
        CGContextAddLineToPoint(context, 15+i*20, 315 );
    }
    CGContextStrokePath(context);
    
    
    CGColorRef redC = [[UIColor redColor] CGColor];
    
    CGContextRef context2 = UIGraphicsGetCurrentContext();

    
    CGContextSetFillColorWithColor(context2, redC);
    for(int i=0; i<14;i++){
        
        for(int j=0; j<14; j++){
        NSLog(@"Life status of grid[%i][%i] is %i",i,j,grid[i][j].life_status);
        if (grid[i][j].life_status)//cell is alive
            {
                NSLog(@"I am in the drawrect method");
                CGContextFillEllipseInRect(context2, CGRectMake(20+j*20, 40+i*20, 10, 10));
            }
        }
    }
    num++;
    NSLog(@"Num %i, cellTouch %i", num, cellTouch);
}


- (void) x_val:(int) x y_val: (int) y
    {
        int i = 0;
        int j = 0;
        //NSLog(@"I need to find which cell was pressed with these coords %i, %i",x, y);
        //do some math here to get the i and j of the cell then toggle that cells life status
        
        //if y_click_coordinate is between y and y+20 your i will be a multiple of n plus x
        
        NSLog(@"Hekko");
        
        if(x>15&&x<295&&y>35&&y<315)
        {
            j=(y-35)/20;
            i = (x-15)/20;
            grid[i][j].life_status = !grid[i][j].life_status;
            NSLog(@"Life status of grid[%i][%i] is %i",i,j,grid[i][j].life_status);
            
        }
        
        else
        {
            NSLog(@"You can only create and destroy life in the grid!!");

        }
    }

- (void) destroy_all_life
    {
        for(int i=0; i<14;i++){
            
            for(int j=0; j<14; j++)
            {
                grid[i][j].life_status = false;
            }
        }
    }

- (void) update_cell_statuses
{
    int live_neighbors;
    for (int i = 0; i < 14; i++) {
        for (int j = 0; j < 14; j++) {
            //Log.d("thingy","Grid["+i+"]["+j+"]");
            live_neighbors = [self a_val:i b_val:j];
            //live_neighbors = check_neighbors(i,j);
            //if our cell is currently alive then check some more stuff
            if ((grid[i][j]).life_status)
            {
                //Log.d("thongy","Grid["+i+"]["+j+"] has"+live_neighbors+"neighbors");
                //celll dies due to overpoppulation
                if(live_neighbors>3)
                {
                    (grid[i][j]).next_status = false;
                }
                //cell dies if it gets lonely
                else if(live_neighbors<2)
                {
                    (grid[i][j]).next_status = false;
                }
                else if(live_neighbors==2 ||live_neighbors==3 )(grid[i][j]).next_status = true;
            }
            
            //this is for a currently dead cell that will be born next cycle
            if((grid[i][j]).life_status == false && live_neighbors == 3){
                (grid[i][j]).next_status = true;
            }
            
        }
        
    }
    
    for (int i = 0; i < 14; i++) {
        for (int j = 0; j < 14; j++) {
            (grid[i][j]).life_status = (grid[i][j]).next_status;
        }
    }


}

- (int) a_val:(int) a b_val: (int) b
{
    int x =0;
    
    if(a<13 && b<13 && (grid[a+1][b+1]).life_status) x++; //southe
    if(b<13 && (grid[a][b+1]).life_status) x++;//south
    if(a>0 && b<13 && (grid[a-1][b+1]).life_status) x++;//southw
    if(a>0 && (grid[a-1][b]).life_status) x++;//west
    if(b>0 && a>0 && (grid[a-1][b-1]).life_status) x++;//northw
    if(b>0 && (grid[a][b-1]).life_status) x++;//north
    if(a<13 && b>0 && (grid[a+1][b-1]).life_status) x++;//northe
    if(a<13 && (grid[a+1][b]).life_status) x++;//east
    
    
    return x;

}


@end
