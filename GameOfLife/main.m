//
//  main.m
//  GameOfLife
//
//  Created by Hoang on 5/5/17.
//  Copyright (c) 2017 ___SJSULAB___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
