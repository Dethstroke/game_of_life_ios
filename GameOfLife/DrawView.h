//
//  DrawView.h
//  GameOfLife
//
//  Created by Hoang on 5/5/17.
//  Copyright (c) 2017 ___SJSULAB___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawView : UIView
@property (strong, nonatomic) NSArray * data;
- (void) x_val:(int) x y_val: (int) y;//this is for toggle life status
- (void) destroy_all_life;
- (void) update_cell_statuses;
- (int) a_val:(int) a b_val: (int) b; //this is for check neighborrs

@property(nonatomic) int cellTouch;
//@property(nonatomic) NSArray* touchArray;

@end


