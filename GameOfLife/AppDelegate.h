//
//  AppDelegate.h
//  GameOfLife
//
//  Created by Hoang on 5/5/17.
//  Copyright (c) 2017 ___SJSULAB___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
